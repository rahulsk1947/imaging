const __basefolder = "C:/Users/user/Desktop/uploads/images/";
const express = require('express');
const multer = require('multer');
const AWS = require('aws-sdk');
const app = express();
const PORT = 3000;
const AdmZip = require('adm-zip');
const { Storage } = require('@google-cloud/storage');
const nodestorage = new Storage();
const keys = require('./keys.js');
const gm = require('gm');
const fs = require('fs');
const sharp = require('sharp');
const bucket = nodestorage.bucket('imaging');

const s3 = new AWS.S3();

app.use(express.static('public'));

const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, __basefolder)
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname)
	}
});
const upload = multer({ storage: storage });

AWS.config.update({
	accessKeyId: keys.iam_access_id,
	secretAccessKey: keys.iam_secret,
	region: 'ap-southeast-1',
});


app.post('/upload', upload.array('file'), (req, res) => {
	console.log('req.files:', req.files);
	var returnjson = "";
	var imagelist = [];
	(req.files).forEach(function (file) {
		console.log('req:', file.mimetype);

		//for zip files
		if (file.mimetype == 'application/zip') {

			var zip = new AdmZip(__basefolder  + file.originalname);
			zip.extractAllTo(__basefolder, true);
			var zipEntries = zip.getEntries();
			zipEntries.forEach(function (entry) {
				imagelist.push(entry.name, __basefolder  + file.originalname + '/' + entry.name);
			});

		} else if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
			//for images
			imagelist.push(file.originalname, __basefolder  + file.originalname);

			var sizeOf = require('image-size');
			sizeOf(__basefolder + file.originalname, function (err, dimensions) {
				if (dimensions.width > 128 && dimensions.height > 128) {
					console.log('more than 128');
					const resize_2 = resizeImage(__basefolder, file.originalname, dimensions, 2);
					const resize_4 = resizeImage(__basefolder, file.originalname, dimensions, 4);
					imagelist.push([resize_2, file.originalname]);
					imagelist.push([resize_4, file.originalname]);
				}
			});
			uploadFile(file);
		} else {
			returnjson += "invalid file format.";
			imagelist.push("invalid file format.");
		}
	});
	res.json(imagelist);

	const file = req.file;
	console.log('file:', file);
});

//resize images. reduce image size as per divisor value.
async function resizeImage(folder, fileName, dimensions, divisor) {

	const resize_image_outputpath = __basefolder + fileName + '_resize_' + divisor.toString() + '.jpg';

	sharp(folder + fileName)
		.resize(dimensions.width / divisor, dimensions.height / divisor)
		.toFile(resize_image_outputpath, function (err) {
			if (err) {
				console.log("Error: ", err);
			}
		});
	return resize_image_outputpath;
};

//upload file to the cloud
const uploadFile = (fileName) => {
	const folder = __basefolder;
	const file = fileName.originalname;

	const params = {
		Bucket: 'servicerocket',
		Key: (folder + file),
		ACL: 'public-read',
		Body: file
	};
	console.log("Folder name: " + folder);
	console.log("File: " + file);

	s3.putObject(params, function (err, data) {
		if (err) {
			console.log("Error: ", err);
		} else {
			console.log(data);
		}
	});
};


app.listen(PORT, () => {
	console.log('Listening at ' + PORT);
});